#include <benchmark/benchmark.hpp>

#include <stack.h>

#include <iostream>

Stack<int> stack;

void StressPush(benchmark::State& state) {
    if (state.thread_index == 0) {
        stack.Clear();
    }
    while (state.KeepRunning()) {
        stack.Push(0);
    }
}

void StressPushPop(benchmark::State& state) {
    if (state.thread_index == 0) {
        stack.Clear();
    }

    if (state.thread_index < state.range(0)) {
        while (state.KeepRunning()) {
            stack.Push(0);
        }
    } else {
        int value;
        while (state.KeepRunning()) {
            (void)stack.Pop(&value);
        }
    }
}

BENCHMARK(StressPush)->Threads(8);
BENCHMARK(StressPushPop)->Threads(50)->Arg(42)->UseRealTime();
BENCHMARK(StressPushPop)->Threads(80)->Arg(75)->UseRealTime();
BENCHMARK(StressPushPop)->Threads(20)->Arg(15)->UseRealTime();
BENCHMARK(StressPushPop)->Threads(60)->Arg(50)->UseRealTime();

BENCHMARK_MAIN();
