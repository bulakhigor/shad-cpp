#include <thread>
#include <mutex>
#include <vector>
#include <set>
#include <condition_variable>
#include <cstdint>

#include <boost/asio.hpp>

class S3Server {
public:
    S3Server(boost::asio::io_service& io_service,
             const boost::asio::ip::tcp::endpoint& at,
             int max_active_conn) {}

    void Start();
    void Shutdown();

    int32_t ProcessedQueryCount();

private:
};
